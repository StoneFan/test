//
//  BananaManager.h
//  simpson-evolution
//
//  Created by ENZO YANG on 13-3-27.
//  Copyright (c) 2013年 YouMi Mobile Co. Ltd. All rights reserved.
//

#ifndef __simpson_evolution__BananaManager__
#define __simpson_evolution__BananaManager__

class BananaManager
{
public:
    static BananaManager *sharedInstance();
    BananaManager();
    
    void bananaFromYouMiArrive();
    
    int bananaCount();
    int giveOutABanana(); // 有返回1， 没有返回0
    void buyBanana();
    void checkIfBananaArrived();
    
    void storeBananas();
    
private:
    int _bananaCount;
};

#endif /* defined(__simpson_evolution__BananaManager__) */
